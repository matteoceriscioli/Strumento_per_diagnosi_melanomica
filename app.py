# -*- coding: cp1252 -*-
######## Strumento per la diagnosi di melanomi basato su un classificatore allenato con Tensorflow #########
#
# Autore: Matteo Ceriscioli
# Date: 9/6/2018
# Descrizione: 
# Questo programma usa un classificatore basato su reti neurali artificiali involute.
# Carica il classificatore ed esegue delle analisi sulle immagini in upload.
# Intorno agli elementi della cute individuati vengono disegnati dei riquadri.

## Tutto il codice sottoposto a licenza MIT � accessibile sul mio profilo GitLab:
## https://gitlab.com/matteoceriscioli/Strumento_per_diagnosi_melanomica
from flask import Flask, flash, request, redirect, url_for, render_template
from flask_uploads import UploadSet, configure_uploads, IMAGES
from werkzeug.datastructures import FileStorage
import os
import cv2
import numpy as np
import tensorflow as tf
import sys
import jinja2
from subprocess import call

# Aggiunta del percorso dell'API Object_recognition di Tensorflow alla variabili d'ambiente PYTHONPATH
call("export PYTHONPATH=$PYTHONPATH:/home/tensorflow1/models/research:/home/tensorflow1/models/research/slim", shell=True)

from utils import label_map_util
from utils import visualization_utils as vis_util

def load_graph(frozen_graph_filename):
    # Si carica il grafico inferenziale in memoria
    with tf.gfile.GFile(frozen_graph_filename, "rb") as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())

    # Si inserisce il grafico in un istanza graph_def di Tensorflow
    with tf.Graph().as_default() as graph:
        tf.import_graph_def(graph_def, name='')
    return graph

# Inizializzazione costanti (percorsi, numero classi)
MODEL_NAME = 'inference_graph'
CWD_PATH = os.getcwd()

PATH_TO_CKPT = os.path.join(CWD_PATH,MODEL_NAME,'frozen_inference_graph.pb')
PATH_TO_LABELS = os.path.join(CWD_PATH,'labelmap.pbtxt')

NUM_CLASSES = 2

# Caricamento delle classi da identificare
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)
print(category_index)

detection_graph=load_graph(PATH_TO_CKPT)
sess = tf.Session(graph=detection_graph)

# Configurazione del motore di Tensorflow
image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
num_detections = detection_graph.get_tensor_by_name('num_detections:0')

app = Flask(__name__)

# Chiave segreta (da cambiare ad ogni istanza del software) di Flask
app.secret_key = '######'

# Inizializzazione del gestore di upload delle immagini
nevi = UploadSet('photos', IMAGES)
app.config['UPLOADED_PHOTOS_DEST'] = 'static'
configure_uploads(app, nevi)


# Index
@app.route("/")
def main():
    return render_template('camera_input.html')

# Upload
@app.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST' and 'nevo' in request.files:
        # Salvataggio dell'immagine caricata sul disco
        filename = nevi.save(request.files['nevo'])

        # Creazione di un immagine opencv
        image = cv2.imread('static/'+filename)
        image_expanded = np.expand_dims(image, axis=0)

        # Classificazione dell'immagine da parte di Tensorflow
        (boxes, scores, classes, num) = sess.run([detection_boxes, detection_scores, detection_classes, num_detections],feed_dict={image_tensor: image_expanded})
        vis_util.visualize_boxes_and_labels_on_image_array(image, np.squeeze(boxes), np.squeeze(classes).astype(np.int32), np.squeeze(scores), category_index, use_normalized_coordinates=True, line_thickness=8, min_score_thresh=0.80)

        # Salvataggio su disco dell'immagine analizzata
        cv2.imwrite("static/a"+filename, image)
        
        return render_template('risultato.html', value="a"+filename)
    return redirect(url_for('main'))

# Inizializzazione di Flask
if __name__ == "__main__":
    app.run(host= '0.0.0.0', port=5000, debug=False)
